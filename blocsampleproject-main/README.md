# BlocSampleProject


## Project Overview

  
It is a user-friendly and secure flutter Architecture that aims to simplify the development of any small/big application. The Architecture will provide a convenient experience for users to start flutter development.


  ## Technology Stack
  The App will be developed for the Flutter platform, utilizing the following technologies:
  - Programming Language: Dart v3.0.5
  - SDK: min21 and max33+
  - UI/UX: Flutter widgets with Material Design guidelines
  - Project Architecture: Bloc Pattern, Clean Architecture

  ## Requirement

 - `VS Code:` 
 - `Android Studio:` (Optional)
