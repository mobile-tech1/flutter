import 'package:get_storage/get_storage.dart';

class MyService {
  MyService._();

  static MyService? _instance;

  static MyService get instance => _instance!;

  static Future<MyService> init() async {
    if (_instance == null) {
      await GetStorage.init();
      _instance = MyService._();
    }
    return _instance!;
  }

  bool isLoggedIn() {
    return true;
  }
}
