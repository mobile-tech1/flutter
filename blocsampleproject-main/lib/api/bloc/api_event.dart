sealed class ApiEvent {
  final String apiEndPoints;
  final bool needAuthorization;
  final Map<String, String> extraHeader;

  const ApiEvent({
    required this.apiEndPoints,
    this.needAuthorization = false,
    this.extraHeader = const {},
  });
}

class Get extends ApiEvent {
  final String queryParams;

  const Get(
    String apiEndPoints, {
    this.queryParams = '',
    bool needAuthorization = false,
    Map<String, String> extraHeader = const {},
  }) : super(
          apiEndPoints: apiEndPoints,
          needAuthorization: needAuthorization,
          extraHeader: extraHeader,
        );
}

class Post extends ApiEvent {
  final Map<String, dynamic> body;

  const Post(
    String apiEndPoints,
    this.body, {
    bool needAuthorization = false,
    Map<String, String> extraHeader = const {},
  }) : super(
          apiEndPoints: apiEndPoints,
          needAuthorization: needAuthorization,
          extraHeader: extraHeader,
        );
}

class Put extends ApiEvent {
  final String id;
  final Map<String, dynamic> body;

  const Put(
    String apiEndPoints,
    this.id,
    this.body, {
    bool needAuthorization = false,
    Map<String, String> extraHeader = const {},
  }) : super(
          apiEndPoints: apiEndPoints,
          needAuthorization: needAuthorization,
          extraHeader: extraHeader,
        );
}

class Delete extends ApiEvent {
  final String id;

  const Delete(
    String apiEndPoints,
    this.id, {
    bool needAuthorization = false,
    Map<String, String> extraHeader = const {},
  }) : super(
          apiEndPoints: apiEndPoints,
          needAuthorization: needAuthorization,
          extraHeader: extraHeader,
        );
}
