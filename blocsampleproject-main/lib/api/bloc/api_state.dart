import 'package:equatable/equatable.dart';
import 'package:flutter_demo_project/api/bloc/base_state.dart';

abstract class ApiState extends Equatable implements BaseState {
  const ApiState();
  @override
  List<Object> get props => [];
}

class ApiInitial extends ApiState {}

class ApiSuccess extends ApiState {
  final dynamic data;
  const ApiSuccess({required this.data});

  @override
  List<Object> get props => [data];
}

class ApiLoading extends ApiState {}

class ApiError extends ApiState {
  final String errorMessage;
  const ApiError({required this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}

class ApiConnectionError extends ApiState {
  final String connectionErrorMessage;
  const ApiConnectionError({
    this.connectionErrorMessage = 'No internet connection',
  });

  @override
  List<Object> get props => [connectionErrorMessage];
}
