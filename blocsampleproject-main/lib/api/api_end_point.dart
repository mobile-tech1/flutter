/// This class contains static constants representing various API endpoints used in the application.
/// Each constant represents a specific API endpoint URL or path.
class ApiEndPoint {
  static const String post = 'posts';
}
