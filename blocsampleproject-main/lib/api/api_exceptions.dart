/// Exception class representing a 401 Unauthorized HTTP response.
class UnauthorizedException implements Exception {
  final String message;

  UnauthorizedException(this.message);

  @override
  String toString() => message;
}

/// Exception class representing a 403 Forbidden HTTP response.
class ForbiddenException implements Exception {
  final String message;

  ForbiddenException(this.message);

  @override
  String toString() => message;
}

/// Exception class representing a 404 Not Found HTTP response.
class NotFoundException implements Exception {
  final String message;

  NotFoundException(this.message);

  @override
  String toString() => message;
}

/// Exception class representing a 500 Internal Server Error HTTP response.
class InternalServerErrorException implements Exception {
  final String message;

  InternalServerErrorException(this.message);

  @override
  String toString() => message;
}

/// Exception class representing a 502 Bad Gateway HTTP response.
class BadGatewayException implements Exception {
  final String message;

  BadGatewayException(this.message);

  @override
  String toString() => message;
}

/// Exception class representing a 503 Service Unavailable HTTP response.
class ServiceUnavailableException implements Exception {
  final String message;

  ServiceUnavailableException(this.message);

  @override
  String toString() => message;
}

/// Exception class representing a 400 Bad Request HTTP response.
class BadRequestException implements Exception {
  final String message;

  BadRequestException(this.message);

  @override
  String toString() => message;
}

/// Exception class representing a 409 Conflict HTTP response.
class ConflictException implements Exception {
  final String message;

  ConflictException(this.message);

  @override
  String toString() => message;
}
