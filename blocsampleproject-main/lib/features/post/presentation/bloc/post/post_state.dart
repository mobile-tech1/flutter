part of 'post_bloc.dart';

abstract class PostState extends Equatable implements BaseState {
  const PostState();
  @override
  List<Object> get props => [];
}

class PostInitial extends PostState {}

class ListOfPost extends PostState {
  final List<PostModel> post;

  const ListOfPost({required this.post});

  @override
  List<Object> get props => [post];
}
