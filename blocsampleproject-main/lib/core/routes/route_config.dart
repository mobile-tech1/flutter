import 'package:flutter/material.dart';
import 'package:flutter_demo_project/core/routes/route_path.dart';
import 'package:flutter_demo_project/features/post/presentation/screens/post_detail_screen.dart';
import 'package:flutter_demo_project/main/defer_init.dart';
import 'package:go_router/go_router.dart';

class RouteConfig {
  static GoRouter router(BuildContext context) => GoRouter(
        routes: [
          GoRoute(
            path: RoutePath.intialRoute,
            builder: (context, state) => const DeferScreen(),
          ),
          GoRoute(
            path: RoutePath.postDetail,
            builder: PostDetailScreen.routeBuilder,
          ),
        ],
        errorBuilder: (context, state) => const Scaffold(
          body: Text('data'),
        ),
      );
}
