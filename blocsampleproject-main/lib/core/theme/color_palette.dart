import 'package:flutter/material.dart';

class ColorPalette {
  ///light theme colors

  static const Color primaryColor = Color(0xff123258);
  static const Color secondaryColor = Color(0xfff6f8fb);

  ///dark theme colors
}
