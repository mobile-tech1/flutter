import 'package:flutter/material.dart';
import 'package:flutter_demo_project/core/theme/color_palette.dart';

class AppTheme {
  static ThemeData lightTheme = ThemeData(
    colorScheme: const ColorScheme.light().copyWith(
      primary: ColorPalette.primaryColor,
      secondary: ColorPalette.secondaryColor,
    ),
  );

  static ThemeData darkTheme = ThemeData(colorScheme: const ColorScheme.dark());
}
